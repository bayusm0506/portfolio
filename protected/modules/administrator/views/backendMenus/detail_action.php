<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Backend
    <small>Details Menus</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> IT Pro</li>
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/backendMenus/admin') ?>"><i class="fa fa-gears"></i> Backend Menu</a></li>
    <li class="active"> Create</li>
  </ol>
</section>
<section class="content">
    <div class="row">
		<div class="col-md-8">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-diamond"></i>
					<h3 class="box-title">Details Data </h3>
					<div class="box-tools">
						<a href="#" class="add-new-data btn btn-primary pull-right"> Create Action </a>
					</div>
				</div>
				<div class="box-body no-padding">
					<div class="add-new-data-form" style="display:none">
						<?php $this->renderPartial('_add_detail_form',array(
						'model'=>$model,
					  )); ?>
					 </div>
					<table id="table-html" class="table">
						<thead>
							<tr>
								<th>No</th>
								<th class="text-center">Nama Aksi</th>
								<th class="text-center">Metode Aksi</th>
								<th colspan="2" class="text-center"></th>
							</tr>
						</thead>
						<tbody>
							<?php $this->renderPartial('_list_item',array(
								'details'=>$details,
								'id' => $id
							)); ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
  $("document").ready(function(){
    var deleteLinks = document.querySelectorAll('#namecheap');

    for (var i = 0; i < deleteLinks.length; i++) {
      deleteLinks[i].addEventListener('click', function(event) {
          event.preventDefault();

          var choice = confirm(this.getAttribute('data-confirm'));

          if (choice) {
            var link = window.location.href = this.getAttribute('href');
            window.setTimeout(window.location.href = link,50000);
          }
          // else if(response.status == 'error'){
          //   //Message
          // }else if(response.status == 'info'){
          //   window.setTimeout(window.location.href = response.redirect,50000);
          // }
      });
    }
  });
</script>


<?php 
          Yii::app()->clientScript->registerScript('search', "
          $('.add-new-data').click(function(){
            $('.add-new-data-form').toggle();
            $('input.btn-btn-data').val('Create');
            $('input[name=id_details]').val('0');
            $('form#application-register-detail-form')[0].reset();
            return false;
          });
          ");
          ?>