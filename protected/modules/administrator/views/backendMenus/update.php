<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Backend
    <small>Menus</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> IT Pro</li>
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/backendMenus/admin') ?>"><i class="fa fa-gears"></i> Backend Menu</a></li>
    <li class="active"> Update</li>
  </ol>
</section>
<section class="content">
    <div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-diamond"></i>
					<h3 class="box-title">Update Backend Menus</h3>
					<div class="box-tools">
              </div>
				</div>
				<?php $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</section>