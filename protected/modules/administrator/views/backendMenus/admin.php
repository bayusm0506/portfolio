<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Backend
    <small>Menus</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> IT Pro</li>
    <li class="active"> Backend Menu</li>
  </ol>
</section>
<section class="content">
    <div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-tools">
						<a href="<?=Yii::app()->createAbsoluteUrl('/administrator/backendMenus/create')?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Create</a>
					</div>
					<i class="fa fa-diamond"></i>
					<h3 class="box-title">Backend Data</h3>
				</div>
					<div class="box-body">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'backend-menus-grid',
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'itemsCssClass' => 'table table-striped table-hover',
							'columns'=>array(
								array(
									'name'=>'id',
									'header'=>'ID',
								),
								array(
									'name'=>'parent_menu',
									'header'=>'Parent Menu',
									'value'=>'backendMenus::model()->findByAttributes(array("id"=>$data["parent_menu"]))->nama_menu',
									'filter'=>CHtml::listdata(backendMenus::model()->findAllByAttributes(array('parent_menu'=>0)),'id','nama_menu'),
									'sortable'=>false,
								),
								array(
									'name'=>'link_url',
									'header'=>'Link URL',
									'sortable'=>false,
								),
								array(
									'name'=>'status',
									'header'=>'Status',
									'value'=>'BackendMenus::status($data->status)',
									'filter'=>BackendMenus::status(),
									'sortable'=>false,
								),
								array(
									'header'=>'ACTION',
									'class'=>'CButtonColumn',
								    'template'=>'{update} {delete} {manager}',
								    'buttons'=>array (
								        'update'=>array(
					                      	'label'=>' ',
					                      	'options' => array('class'=>'fa fa-edit fa-lg', 'data-toggle'=>'tooltip', 'data-placement'=>'bottom', 'title'=>'Perbarui Data'),
					                      	'imageUrl'=>false,
					                    ),
					                    'delete'=>array(
					                      	'label'=>' ',
					                      	'options' => array('class'=>'fa fa-trash-o fa-lg', 'data-toggle'=>'tooltip', 'data-placement'=>'bottom', 'title'=>'Hapus Data'),
					                      	'imageUrl'=>false,
					                    ),
								        'manager'=>array(
								            'label'=>' ',
								            'url'=>'Yii::app()->createUrl("/administrator/backendMenus/manageAction/", array("id"=>$data->id))',
								            'options' => array('class'=>'fa fa-gears fa-lg', 'data-toggle'=>'tooltip', 'data-placement'=>'bottom', 'title'=>'Manage Data'),
								            'imageUrl'=>false,
								        ),
									),
								),
							),
						)); 
					?>
				</div>
		   </div>
		</div>
	</div>
</section>
