<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    User
    <small>Role</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> IT Pro</li>
    <li class="active"> User Role</li>
  </ol>
</section>
<section class="content">
    <div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="  box-tools">
						<a href="<?=Yii::app()->createAbsoluteUrl('/administrator/userRole/create')?>"  class=" btn btn-primary btn-sm "><i class="fa fa-plus"></i> Create</a>
					</div>
					<i class="fa fa-diamond"></i>
					<h3 class="box-title">Role Data</h3>
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Kode Unique</th>
							<th>Role</th>
							<th>Url Address</th>
							<th colspan="2">Action</th>
						</tr>
					</thead>
					<tbody>
					<?php 
						foreach( $model as $p ){
							?>
								<tr>
									<td><?=$p->id?></td>
									<td><?=$p->kode?></td>
									<td><?=$p->nama_akses?></td>
									<td><?=$p->alamat_utama?></td>
									<td><a class="btn btn-danger" href="<?=Yii::app()->createAbsoluteUrl('administrator/userRole/menuAkses/id/'.$p->id)?>" data-toggle="tooltip" data-placement="bottom" title="Setting Access Menu"><i class="fa fa-gears"></i></a> - 
										<a class="btn btn-primary" href="<?=Yii::app()->createAbsoluteUrl('administrator/userRole/update/id/'.$p->id)?>" data-toggle="tooltip" data-placement="bottom" title="Update Data"><i class="fa fa-edit"></i>
									</td>
								</tr>
							<?php
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>