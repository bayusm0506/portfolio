<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Front
    <small>End</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> Front End</li>
     <li><i class="fa fa-gears"></i> Layout</li>
    <li class="active"> Form</li>
  </ol>
</section>
<section class="content">
    <div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-diamond"></i>
					<h3 class="box-title">Form </h3>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_layout', array('model'=>$model)); ?>
				</div>
			</div>
		</div>
	</div>
</section>