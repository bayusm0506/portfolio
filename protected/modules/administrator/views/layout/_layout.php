<?php
/* @var $this TableKegiatanController */
/* @var $model TableKegiatan */
/* @var $form CActiveForm */
?>
 
<div class="form">
  <div class="box-body">
    <?php $form=$this->beginWidget('CActiveForm', array(
      'id'=>'layout-form',
      'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form'
      ),
      // Please note: When you enable ajax validation, make sure the corresponding
      // controller action is handling ajax validation correctly.
      // There is a call to performAjaxValidation() commented in generated controller code.
      // See class documentation of CActiveForm for details on this.
      'enableAjaxValidation'=>false,
    )); ?>
 
      <p class="note">Field <span class="required">*</span> Harus diisi.</p>

      <div class="form-group">
          <?php echo $form->labelEx($model,'layout',array('class'=>'col-sm-2 control-label')); ?>

          <div class="col-sm-10">
            <select class="select2-element form-control" name="Layout[id]">
              <?php foreach(Layout::items_dropdown() as $p ){ ?>
                <option <?=($p['status'] == 1 ? 'selected' : '')?> value="<?=$p['id']?>"><?=$p['layout']?></option>
              <?php } ?>
            </select>
            <div id="tooltip_container"></div>
            <?php echo $form->error($model,'layout'); ?>
          </div>
        </div>
  </div>
  <div class="box-footer">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah',array('id'=>'simpanmenusaya','class'=>'btn btn-primary','name'=>'simpanmenu')); ?>
  </div>
  <?php $this->endWidget(); ?>
 
</div><!-- form -->
<script>
$("document").ready(function(){
  // $("form#layout-form").submit(function(){
  //   var _form = $(this);
  //   Ajax.run(_form.attr('action'),'POST',_form.serialize(),function(response){
  //     // Pesan
  //     if(response.status == 'error'){
  //       //Message
  //     }else if(response.status == 'info'){
  //       //window.setTimeout(window.location.href = response.redirect,50000);
  //     }
  //   });
  //   return false;
  // });
})
</script>