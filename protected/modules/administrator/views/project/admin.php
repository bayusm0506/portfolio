<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Front 
    <small>End</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> Front End</li>
    <li class="active"> Project</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-8">
      <!-- Application buttons -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-search-plus"></i>
          <h3 class="box-title">
            Form Pencarian
          </h3>
        </div>
        <div class="box-body">
        <?php 
          $this->renderPartial('_search',array('model'=>$model)); 
        ?>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <!-- Application buttons -->
      <div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">Export Data</h3>
        </div>
        <div class="box-body">
          <p>Pilih salah satu jenis export data sesuai yang Anda <code>Inginkan</code> dibawah ini :</p>
          <a href="<?=Yii::app()->createAbsoluteUrl('administrator/project/export/tanggal_project/'.$model->tanggal_project.'/dibuat_oleh/'.$model->dibuat_oleh)?>" class="btn btn-app" data-toggle="tooltip" data-placement="bottom" title="Export Excel">
            <i class="fa fa-file-excel-o"></i> Excel
          </a>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <i class="fa fa-database"></i> 
          <h3 class="box-title">Data Project</h3>
          <div class="box-tools">
            <?php echo CHtml::link("<i class='fa fa-plus'></i>  Create Project",array('create'),array('class'=>"btn btn-primary btn-sm")) ?>
          </div>
        </div>
        <div class="box-body">
          <?php $this->renderPartial('_rgridview',array('model'=>$model)); ?>
        </div>
      </div>
    
    </div>
  </div>
  <!-- /. row -->
</section>
<?php
	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function () {
	      $('.search-form').fadeToggle('slow');
			return false;
		});
	");
?>