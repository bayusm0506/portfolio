<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */
?>
<div class="form">
	<div class="box-body">
    	<?php $form=$this->beginWidget('CActiveForm', array(
	      'action'=>Yii::app()->createUrl($this->route),
	      'method'=>'get',    
	      'htmlOptions'=>array(
	        'class'=>'form-horizontal',
	        'role'=>'form'
	      ),
	    )); ?>
	    <div class="row">
	      	<div class="col-lg-4">
		        <?php echo $form->label($model,'nama_project'); ?>
		        <div class="input-group">
			     	<div class="input-group-addon">
			        	<i class="fa fa-id-card"></i>
			      	</div>
			      	<?php echo $form->textField($model,'nama_project',array('class'=>'form-control','placeholder'=>'Nama Project')); ?>
			    </div>
	      </div>
	      <div class="col-lg-4">
	        	<?php echo $form->labelEx($model,'tanggal_pencarian'); ?>
			    <div class="input-group">
			     	<div class="input-group-addon">
			        	<i class="fa fa-calendar"></i>
			      	</div>
			      	<?php echo $form->textField($model,'tanggal_project',array('class'=>'form-control datepicker','placeholder'=>'Tanggal Project')); ?>
			    </div>
	      </div>
	      <div class="col-lg-4">
	        	<?php echo $form->labelEx($model,'dibuat_oleh'); ?>
			    <div class="input-group">
			     	<div class="input-group-addon">
			        	<i class="fa fa-users"></i>
			      	</div>
			      	<select class="select2-element form-control" name="Project[dibuat_oleh]">
			            <option>Pilih Users</option>
			            <?php foreach(User::model()->findAll() as $p ){ ?>
			              <option <?=($model->dibuat_oleh == $p['id'] ? 'selected' : '')?> value="<?=$p['id']?>"><?=$p['nama_lengkap']?></option>
			            <?php } ?>
			        </select>
			    </div>
	      </div>
	    </div>
	</div>
	<div class="box-footer">
		<button class="btn btn-primary btn-sm" name="yt0" type="submit">Cari <i class="fa fa-search"></i></button>
	</div>
	<?php $this->endWidget(); ?>
</div>
<script>
$("document").ready(function(){
  $(".datepicker").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
  });
})
</script>