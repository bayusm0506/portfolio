<style>
    /* FROM HTTP://WWW.GETBOOTSTRAP.COM
     * Glyphicons
     *
     * Special styles for displaying the icons and their classes in the docs.
     */

    .bs-glyphicons {
      padding-left: 0;
      padding-bottom: 1px;
      margin-bottom: 20px;
      list-style: none;
      overflow: hidden;
    }

    .bs-glyphicons li {
      float: left;
      width: 25%;
      height: 115px;
      padding: 10px;
      margin: 0 -1px -1px 0;
      font-size: 12px;
      line-height: 1.4;
      text-align: center;
      border: 1px solid #ddd;
    }

    .bs-glyphicons .glyphicon {
      margin-top: 5px;
      margin-bottom: 10px;
      font-size: 24px;
    }

    .bs-glyphicons .glyphicon-class {
      display: block;
      text-align: center;
      word-wrap: break-word; /* Help out IE10+ with class names */
    }

    .bs-glyphicons li:hover {
      background-color: rgba(86, 61, 124, .1);
    }

    @media (min-width: 768px) {
      .bs-glyphicons li {
        width: 12.5%;
      }
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Front
    <small>End</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/project/admin') ?>"><i class="fa fa-life-ring"></i> Project</a></li>
    <li><i class="fa fa-gears"></i> Project Setting</li>
    <li class="active"> Upload</li>
  </ol>
</section>
<section class="content">
  <div class="callout callout-info">
    <h4>Tips Upload!</h4>

    <p>1. Required File png, jpeg, gif</p>
    <p>2. Required File size upload 2 Mb</p>
    <p>3. Required File upload : Width : 1110 pixels and Height : 529 pixels for good quality</p>
  </div>

  <!-- Default box -->
  <div class="box">
    <div class="row">
      <?php if (Yii::app()->user->hasFlash('success')): ?>
      <div class="col-md-12">
          <div class="alert alert-success">
            <h4><i class="fa fa-info"></i> Note: </h4>
              <?php echo Yii::app()->user->getFlash('success'); ?>
          </div>
      </div>
      <?php endif; ?>
    </div>
    <div class="box-header with-border">
      <i class="fa fa-image fa-lg"></i> 
      <h3 class="box-title">Upload Photos Project</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fa fa-times"></i></button>
      </div>
      <div class="box-tools">
        <?php echo CHtml::link("Data Project",array('admin'),array('class'=>"btn btn-primary btn-sm")) ?>
      </div>
    </div>
    <div class="box-body">
      Browse your photos in here
    </div>
    <?php $this->renderPartial('_upload', array('model'=>$model)); ?> 
  </div>
  <!-- /.box -->
<!-- row -->
      <div class="row">
        <div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
            
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-green">
                    <!-- 3 Jan. 2014 -->
                    Gallery
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-camera bg-purple"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> Uploaded :
                  <?php
                    $time = Project::getImageTimeLast($model->id);
                    $timeLast = ($time['imagetime']);
                    echo Project::time_elapsed_string($timeLast);
                  ?>
                </span>
                <h3 class="timeline-header">Foto Terkait</h3>
                <div class="timeline-body">
                    <div class="tab-pane" id="glyphicons">

                        <ul class="bs-glyphicons">
                          <?php 
                              $xx = ProjectDetail::getprojectdetail($model->id); 
                              foreach ($xx as $s) {
                                $id_detail = $s['gambar'];
                                $nama_file = $s['nama_file'];
                                $project_id = $s['project_id'];
                          ?>
                          <li>
                            <a href="<?php echo Yii::app()->baseUrl; ?>/administrator/project/ImageDetail/?file=<?php echo $s['nama_file'];?>&id=<?php echo $model->id; ?>" target="_blank">
                              <img src="<?php echo Yii::app()->baseUrl; ?>/administrator/project/ImageDetail/?file=<?php echo "thumb_".$s['nama_file'];?>&id=<?php echo $model->id; ?>" alt="<?php echo $s['nama_project']; ?>" class="margin" style="width: 90px; height: 60px;"/>
                            </a>
                            <a id="namecheap" href="<?= Yii::app()->createUrl("/administrator/project/deleteimage",array("id"=>$id_detail, 'project_id'=>$project_id, 'nama_file'=>$nama_file)); ?>" title="Delete" data-confirm="Are you sure to Delete this item?"><span class="glyphicon glyphicon-trash" style="font-size: 14px;"></span></a> -
                            <a id="namecheap" href="<?= Yii::app()->createUrl("/administrator/project/primaryimage",array("id"=>$id_detail, 'project_id'=>$project_id)); ?>" title="Primary Key" data-confirm="Are you sure to Avatar to be active this item?"><span class="glyphicon glyphicon-star" style="font-size: 14px;"></span></a>                          
                          </li>
                          <?php
                              }
                          ?>
                        </ul>
                      </div>
                </div>
                
              </div>
            </li>
            <!-- END timeline item -->
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</section>

<script>
  $("document").ready(function(){
    var deleteLinks = document.querySelectorAll('#namecheap');

    for (var i = 0; i < deleteLinks.length; i++) {
      deleteLinks[i].addEventListener('click', function(event) {
          event.preventDefault();

          var choice = confirm(this.getAttribute('data-confirm'));

          if (choice) {
            var link = window.location.href = this.getAttribute('href');
            window.setTimeout(window.location.href = link,50000);
          }
          // else if(response.status == 'error'){
          //   //Message
          // }else if(response.status == 'info'){
          //   window.setTimeout(window.location.href = response.redirect,50000);
          // }
      });
    }
  });
</script>