<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Front 
    <small>End</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> Contact</li>
    <li class="active"> Contact</li>
  </ol>
</section>
<section class="content">
    <div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-database"></i> 
					<div class="  box-tools">
						<a href="<?=Yii::app()->createAbsoluteUrl('/administrator/contact/create')?>"  class="btn btn-primary"><i class="fa fa-plus"></i> Create</a>
					</div>
					<h3 class="box-title">Contact Data</h3>
				</div>
				<div class="box-body">
					
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'skill-grid',
						'dataProvider'=>$model->search(),
						'filter'=>$model,
						'itemsCssClass' => 'table table-bordered table-hover',
						'columns'=>array(
							array(
								'name'=>'id',
								'header'=>'ID',
							),
							array(
								'name'=>'name',
								'header'=>'Name',
								'sortable'=>false,
							),
							array(
								'name'=>'email',
								'header'=>'Email',
								'sortable'=>false,
							),
							array(
								'name'=>'subject',
								'header'=>'Subject',
								'sortable'=>false,
							),
							array(
								'name'=>'message',
								'header'=>'Message',
								'sortable'=>false,
							),
							array('class'=>'CButtonColumn',
									'header'=>'Action',
								    'template'=>'{update} {delete}',
								    'buttons'=>array (
								        'update'=> array(
								            'label'=>'',
								            'imageUrl'=>false,
								            'options' => array('class'=>'fa fa-edit fa-lg', 'data-toggle'=>'tooltip', 'data-placement'=>'bottom', 'title'=>'Perbarui Data'),
								        ),
								        'delete'=>array(
								            'label'=>'',
								            'imageUrl'=>false,
								            'options' => array('class'=>'fa fa-trash-o fa-lg', 'data-toggle'=>'tooltip', 'data-placement'=>'bottom', 'title'=>'Hapus Data'),
								        ),
								    ),
								),
						),
					)); ?>
			</div>
		</div>
		</div>
	</div>
</section>