<?php
/* @var $this TableKegiatanController */
/* @var $model TableKegiatan */
/* @var $form CActiveForm */
?>
 
<div class="form">
  <div class="box-body">
    <?php $form=$this->beginWidget('CActiveForm', array(
      'id'=>'contact-form',
      'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form'
      ),
      // Please note: When you enable ajax validation, make sure the corresponding
      // controller action is handling ajax validation correctly.
      // There is a call to performAjaxValidation() commented in generated controller code.
      // See class documentation of CActiveForm for details on this.
      'enableAjaxValidation'=>false,
    )); ?>
 
      <p class="note">Field <span class="required">*</span> Harus diisi.</p>
 
      <div class="form-group">
        <?php echo $form->labelEx($model,'name',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'name',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'name'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'email',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'email',array('class'=>'form-control','size'=>10,'maxlength'=>25)); ?>
          <?php echo $form->error($model,'email'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'subject',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'subject',array('class'=>'form-control','size'=>10,'maxlength'=>25)); ?>
          <?php echo $form->error($model,'subject'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'message',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textArea($model,'message',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
          <?php echo $form->error($model,'message'); ?>
        </div>
      </div>
  </div>
  <div class="box-footer">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah',array('id'=>'simpanmenusaya','class'=>'btn btn-primary','name'=>'simpanmenu')); ?>
  </div>
  <?php $this->endWidget(); ?>
 
</div><!-- form -->
<script>
$("document").ready(function(){
  $("form#contact-form").submit(function(){
    var _form = $(this);
    Ajax.run(_form.attr('action'),'POST',_form.serialize(),function(response){
      // Pesan
      if(response.status == 'error'){
        //Message
      }else if(response.status == 'info'){
        window.setTimeout(window.location.href = response.redirect,50000);
      }
    });
    return false;
  });
})
</script>