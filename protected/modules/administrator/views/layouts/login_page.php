<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/x-icon" href="<?= Yii::app()->request->baseUrl; ?>/asset/images/icon.png" />
	<title>Login Administrator - Bayu Setra Maulana</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl; ?>/vendors/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl; ?>/vendors/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl; ?>/vendors/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl; ?>/vendors/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl; ?>/vendors/assets/css/message.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl; ?>/vendors/plugins/iCheck/square/blue.css">
	<script src="<?= Yii::app()->theme->baseUrl; ?>/vendors/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?= Yii::app()->theme->baseUrl; ?>/vendors/assets/jquery/ajax.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<script type="text/javascript">
		$(function() {
		    $('.close').click(function() {
		        $('.message-container').hide();
		    });
		});
	</script>
</head>
<body class="hold-transition login-page">
<div class="messsage-container" style="display:none">
	<div class="loading-temp loading-show"></div>
	<div class="center-loading"><a href="#" style="font-size:24px" class="btn btn-default btn-sm">Proses <i class="fa fa-spinner fa-spin" style="font-size:24px"></i></a></div>
</div>
<div class="message-container" style="display:none">
	<div class="center-message">
	 <div class="alert alert-danger" id="container-m" role="alert"> <button id="remove-message" type="button"  class="close"><i class="fa fa-remove"></i></button> <h4 id="t-message"></h4> <p id="m-message"></p></div> 
	</div>
</div>
	<?php echo $content; ?>
<!-- /.login-box -->

<!-- jQuery 3 -->
<!-- Bootstrap 3.3.7 -->
<script src="<?= Yii::app()->theme->baseUrl; ?>/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= Yii::app()->theme->baseUrl; ?>/vendors/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>

</body>
</html>