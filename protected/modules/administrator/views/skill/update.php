<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Front
    <small>End</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> Skill</li>
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/skill/admin') ?>"><i class="fa fa-gears"></i> Skill Setting</a></li>
    <li class="active"> Update</li>
  </ol>
</section>

<section class="content">
    <div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-diamond"></i>
					<h3 class="box-title">Update Skill</h3>
				</div>
				<?php $this->renderPartial('_form', array('model'=>$model)); ?>
			</div>
		</div>
	</div>
</section>