<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Profile
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> Profile</li>
    <li class="active"><i class="fa fa-gears"></i> Settings</li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <?php if(Yii::app()->user->hasFlash('success')){ ?>
          <div class="col-md-12 closeyoureyes">
              <div class="alert alert-success">
                <button id="remove-message" type="button" class="close"><i class="fa fa-remove"></i></button>
                <h4><i class="fa fa-info"></i> Note: </h4>
                  <?php echo Yii::app()->user->getFlash('success'); ?>
              </div>
          </div>
        <?php }else if(Yii::app()->user->hasFlash('error')){ ?>
          <div class="col-md-12">
              <div class="alert alert-danger">
                <button id="remove-message" type="button" class="close"><i class="fa fa-remove"></i></button>
                <h4><i class="fa fa-info"></i> Note: </h4>
                  <?php echo Yii::app()->user->getFlash('error'); ?>
              </div>
          </div>
        <?php } ?>
    	<div class="col-md-12">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-black" style="background: url('<?= Yii::app()->theme->baseUrl; ?>/vendors/dist/img/photo1.png') center center;">
            	<?php
	                $Data = Home::model()->findByPk(1);
	                if( !empty($Data)){
	                  $header_ = $Data->header;
	                  $skill_ = $Data->skill;
	                }
	            ?>
              <h3 class="widget-user-username"><?= $header_; ?></h3>
              <h5 class="widget-user-desc"><?= $skill_; ?></h5>
            </div>
            <div class="widget-user-image">
            	<?php
	                $profile_ = ProfileBsm::model()->findByPk(1);
	                if( !empty($profile_)){
	                  $profile__ = $profile_->image;
	                }
	            ?>
              <img class="img-circle" src="<?= Yii::app()->request->baseUrl; ?>/upload/photo/<?= $profile__; ?>" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <!-- <h5 class="description-header">3,200</h5>
                    <span class="description-text">SALES</span> -->
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">13,000</h5>
                    <span class="description-text">FOLLOWERS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <!-- <h5 class="description-header">35</h5>
                    <span class="description-text">PRODUCTS</span> -->
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <!-- <form action="<?php // echo Yii::app()->createAbsoluteUrl("/administrator/homePage/profile"); ?>" method="POST"> -->
                <?php 
                  $form=$this->beginWidget(
                    'CActiveForm', array(
                        'id'=>'profile-form',
                        'htmlOptions'=>array(
                            'enctype'=>'multipart/form-data'),'enableAjaxValidation'=>false
                        )
                    );
                ?>
	                <div class="col-sm-6">
	                  <div class="description-block">
                  		<div class="form-group">
		                  	<?php echo CHtml::activeFileField($model,'image',array('id'=>'browse')); ?>
                    </div>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
	                <div class="col-sm-6">
	                  <div class="description-block">
	                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Update',array('id'=>'simpanmenusaya','class'=>'btn btn-primary','name'=>'simpanmenu')); ?>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
            	<?php $this->endWidget(); ?>
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
	</div>
</section>
