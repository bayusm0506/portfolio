<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Front
    <small>End</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> Front End</li>
     <li><i class="fa fa-gears"></i> Settings</li>
    <li class="active"> Form</li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <?php if(Yii::app()->user->hasFlash('success')){ ?>
          <div class="col-md-12 closeyoureyes">
              <div class="alert alert-success">
                <button id="remove-message" type="button" class="close"><i class="fa fa-remove"></i></button>
                <h4><i class="fa fa-info"></i> Note: </h4>
                  <?php echo Yii::app()->user->getFlash('success'); ?>
              </div>
          </div>
        <?php }else if(Yii::app()->user->hasFlash('error')){ ?>
          <div class="col-md-12">
              <div class="alert alert-danger">
                <button id="remove-message" type="button" class="close"><i class="fa fa-remove"></i></button>
                <h4><i class="fa fa-info"></i> Note: </h4>
                  <?php echo Yii::app()->user->getFlash('error'); ?>
              </div>
          </div>
        <?php } ?>
        <div class="col-md-12">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              <?php
                  $Data = Home::model()->findByPk(1);
                  if( !empty($Data)){
                    $header_ = $Data->header;
                    $skill_ = $Data->skill;
                  }
              ?>
              <h3 class="widget-user-username">Logo - <?= $header_; ?></h3>
              <h5 class="widget-user-desc"><?= $skill_; ?></h5>
            </div>
            <div class="widget-user-image">
              <?php
                $logo_ = Home::model()->findByPk(1);
                if( !empty($logo_)){
                  $logo__ = $logo_->logo;
                }
              ?>
              <img class="img-circle" src="<?= Yii::app()->request->baseUrl; ?>/upload/photo/<?= $logo__; ?>" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <!-- <h5 class="description-header">3,200</h5>
                    <span class="description-text">SALES</span> -->
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">13,000</h5>
                    <span class="description-text">FOLLOWERS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <!-- <h5 class="description-header">35</h5>
                    <span class="description-text">PRODUCTS</span> -->
                  </div>
                  <!-- /.description-block -->
                </div>
                <?php 
                  $form=$this->beginWidget(
                    'CActiveForm', array(
                        'id'=>'profile-form',
                        'action' => $this->createUrl('homePage/logo'),
                        'htmlOptions'=>array(
                            'enctype'=>'multipart/form-data'),'enableAjaxValidation'=>false
                        )
                    );
                ?>
                  <div class="col-sm-6">
                    <div class="description-block">
                      <div class="form-group">
                        <input id="ytbrowse" type="hidden" value="" name="HomeLogo[logo]">
                        <input size="60" maxlength="555" id="browse" name="HomeLogo[logo]" type="file">
                        <?php //echo CHtml::activeFileField($model, 'image', array('size'=>60,'maxlength'=>45, 'id'=>'browse')); ?>
                        <?php
                            //echo $form->fileField($model, 'logo', array('size'=>60,'maxlength'=>555, 'id'=>'browse'));
                        ?>
                    </div>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-6">
                    <div class="description-block">
                      <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Update',array('id'=>'simpanmenusaya','class'=>'btn btn-primary','name'=>'simpanmenu')); ?>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
              <?php $this->endWidget(); ?>
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-diamond"></i>
					<h3 class="box-title">Form </h3>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_form_home', array('model'=>$model)); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$("form#profile_form").submit(function(){
    	var _form = $(this);
    	Ajax.run(_form.attr('action'),'POST',_form.serialize(),function(response){
    		if(response.status == 'info' ){
        			// $("#textter").text("Kami Akan segera merespon !!!");
        			// _form[0].reset();
    		}else if(response.status == 'error' ){
        			//$("#textter").text("Email Tidak Valid !!!");
    		}
    	});
    	return false;
	});
</script>