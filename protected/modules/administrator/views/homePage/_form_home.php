<?php
/* @var $this TableKegiatanController */
/* @var $model TableKegiatan */
/* @var $form CActiveForm */
?>
 
<div class="form">
  <div class="box-body">
    <?php $form=$this->beginWidget('CActiveForm', array(
      'id'=>'table-home',
      'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form',
        'enctype' => 'multipart/form-data'
      ),
      // Please note: When you enable ajax validation, make sure the corresponding
      // controller action is handling ajax validation correctly.
      // There is a call to performAjaxValidation() commented in generated controller code.
      // See class documentation of CActiveForm for details on this.
      'enableAjaxValidation'=>false,
    )); ?>
 
      <p class="note">Field <span class="required">*</span> Harus diisi.</p>
 
      <!-- <div class="form-group">
        <?php echo $form->labelEx($model,'image',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo CHtml::activeFileField($model, 'image'); ?>
          <?php echo $form->error($model,'image'); ?>
        </div>
      </div> -->

      <div class="form-group">
        <?php echo $form->labelEx($model,'header',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'header',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'header'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'skill',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'skill',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'skill'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'skill_content',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textArea($model,'skill_content',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
          <?php echo $form->error($model,'skill_content'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'background',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textArea($model,'background',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
          <?php echo $form->error($model,'background'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'content',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textArea($model,'content',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
          <?php echo $form->error($model,'content'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'phone',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'phone',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'phone'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'link_fb',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'link_fb',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'link_fb'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'link_twitter',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'link_twitter',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'link_twitter'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'link_linkedin',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'link_linkedin',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'link_linkedin'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'link_instagram',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'link_instagram',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'link_instagram'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'link_feeds',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'link_feeds',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'link_feeds'); ?>
        </div>
      </div>

      <div class="form-group">
        <?php echo $form->labelEx($model,'stay_touch',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textArea($model,'stay_touch',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
          <?php echo $form->error($model,'stay_touch'); ?>
        </div>
      </div>
      <div class="form-group">
        <?php echo $form->labelEx($model,'blog',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
          <?php echo $form->textField($model,'blog',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
          <?php echo $form->error($model,'blog'); ?>
        </div>
      </div>
  </div>
  <div class="box-footer">
    <?php //echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah',array('id'=>'simpanmenusaya','class'=>'btn btn-primary','name'=>'simpanmenu')); ?>
    <?php echo CHtml::submitButton('Save', array('name' => 'save', 'class'=>'btn btn-primary')); ?>
  </div>
  <?php $this->endWidget(); ?>
 
</div><!-- form -->
<script>
$("document").ready(function(){
  $("form#table-home").submit(function(){
    var _form = $(this);
    Ajax.run(_form.attr('action'),'POST',_form.serialize(),function(response){
      // Pesan
      if(response.status == 'error'){
        //Message
      }else if(response.status == 'info'){
        //window.setTimeout(window.location.href = response.redirect,50000);
      }
    });
    return false;
  });
})
</script>