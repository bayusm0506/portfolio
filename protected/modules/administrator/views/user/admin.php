<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    User 
    <small>Setting</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> User</li>
    <li class="active"> User Setting</li>
  </ol>
</section>
<section class="content">
    <div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-database"></i> 
					<div class="  box-tools">
						<a href="<?=Yii::app()->createAbsoluteUrl('/administrator/user/create')?>"  class="btn btn-primary"><i class="fa fa-plus"></i> Create</a>
					</div>
					<h3 class="box-title">User Data</h3>
				</div>
				<div class="box-body">
					
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'user-grid',
						'dataProvider'=>$model->search(),
						'filter'=>$model,
						'itemsCssClass' => 'table table-bordered table-hover',
						'columns'=>array(
							array(
								'name'=>'username',
								'header'=>'Username',
							),
							array(
								'name'=>'userlevel',
								'header'=>'User Level',
								'value'=>'UserRole::model()->findByAttributes(array("kode"=>$data["userlevel"]))->nama_akses',
								'filter'=>CHtml::listData(UserRole::model()->findAll(),'kode','nama_akses'),
								'sortable'=>false,
							),
							array(
								'name'=>'nik',
								'header'=>'Nik',
								'sortable'=>false,
							),
							array(
								'name'=>'nama_lengkap',
								'header'=>'Nama Lengkap',
								'sortable'=>false,
							),
							array(
								'name'=>'tanggal_daftar',
								'header'=>'Tanggal Daftar',
								'sortable'=>false,
							),
							/*
							'tanggal_ubah',
							'password',
							*/
							 array('class'=>'CButtonColumn',
								    'template'=>'{update} {delete}',
								    'buttons'=>array (
								        'update'=> array(
								            'label'=>'',
								            'imageUrl'=>false,
								            'options' => array('class'=>'fa fa-edit fa-lg', 'data-toggle'=>'tooltip', 'data-placement'=>'bottom', 'title'=>'Perbarui Data'),
								        ),
								        'delete'=>array(
								            'label'=>'',
								            'imageUrl'=>false,
								            'options' => array('class'=>'fa fa-trash-o fa-lg', 'data-toggle'=>'tooltip', 'data-placement'=>'bottom', 'title'=>'Hapus Data'),
								        ),
								        // 'hapus'=>array(
								        //     'label'=>'',
								        //     'url'=>'Yii::app()->createUrl("/administrator/user/delete",array("id"=>$data->id))',
								        //     'options'=>array( 'class'=>'fa fa-remove' ),
								        // ),
								    ),
								),
						),
					)); ?>
			</div>
		</div>
		</div>
	</div>
</section>





