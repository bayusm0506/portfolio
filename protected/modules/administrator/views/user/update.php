<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    User
    <small>Setting</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-life-ring"></i> User</li>
    <li><a href="<?= Yii::app()->createAbsoluteUrl('/administrator/user/admin') ?>"><i class="fa fa-gears"></i> User Setting</a></li>
    <li class="active"> Update</li>
  </ol>
</section>
<section class="content">
    <div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-diamond"></i>
					<h3 class="box-title">Update User</h3>
					<div class="box-tools">
		                <a href="<?=Yii::app()->createAbsoluteUrl('administrator/user/create')?>" class="btn btn-primary"><i class="fa fa-plus"></i> Create User</a>
		            </div>
				</div>
				<?php $this->renderPartial('_form', array('model'=>$model)); ?>
			</div>
		</div>
	</div>
</section>