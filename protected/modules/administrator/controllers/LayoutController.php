<?php

class LayoutController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return RolesMenu::actionRule('layout'); 
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Layout;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Layout']))
		{
			$model->attributes=$_POST['Layout'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Layout']))
		{
			$model->attributes=$_POST['Layout'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Layout;
		if(isset($_POST['Layout']))
		{
			$model->attributes = $_POST['Layout'];

			$sql = "UPDATE t_layout SET status='0'";
			$command = Yii::app()->db->createCommand($sql);
			return $dataReader = $command->queryAll();

			// $sql2 = "UPDATE t_layout SET status='1' WHERE id='$model->id'";
			// $command2 = Yii::app()->db->createCommand($sql2);
			// return $dataReader2 = $command2->queryAll();

			Yii::app()->db->createCommand()->update('t_layout', array(
				'status'=>0,
          	));

          	Yii::app()->db->createCommand()->update('t_layout', array(
				'status'=>1,
          	), 'id=:id', array(':id'=>$model->id));

			// $response = ["status"=>'info','msg'=>'Data Berhasil Disimpan','redirect'=>Yii::app()->createAbsoluteUrl('administrator/layout/')];
			// echo CJSON::encode($response);
			// Yii::app()->end();
			// return;

			$this->redirect(array('layout'));
		}

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Layout('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Layout']))
			$model->attributes=$_GET['Layout'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Layout the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Layout::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Layout $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='layout-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
