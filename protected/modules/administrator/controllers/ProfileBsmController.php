<?php

class ProfileBsmController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return RolesMenu::actionRule('profileBsm'); 
	}

	public function actionIndex()
	{
		$model=$this->loadModel(1);
		$photoLokasi = Yii::getPathOfAlias('webroot').Yii::app()->params['photopic'];
        if(isset($_POST['ProfileBsm'])){
            $model->attributes=$_POST['ProfileBsm'];
            if (!empty($_FILES['ProfileBsm']['name'])) {
                $random = rand(0,99999999999);
                $tempFile = $_FILES['ProfileBsm']['tmp_name']['image']; 
                $targetPath = $photoLokasi;  
                $file_name = $random.'_'.str_replace(' ' ,'_' ,$_FILES['ProfileBsm']['name']['image']);
                $targetFile =  $targetPath.$file_name;
                if (file_exists($targetFile)) {
                	try {
                      echo "Success...";
                    } catch(ErrorException $ex) {
                       echo "Error: " . $ex->getMessage();
                    }
                }
                if(move_uploaded_file($tempFile,$targetFile)){
                    $path = $targetPath.$file_name;
                    $target = Yii::app()->simpleImage->load($targetFile);
                    $target->crop(128,128);
                    $target->save($path);

                    $round_thumb = $targetPath.'profile_'. $file_name;
					$round = Yii::app()->simpleImage->load($targetFile);
					$round->crop(400,600);
					$round->save($round_thumb);

                    $model->image = $file_name;
                    if($model->save()){
                    	Yii::app()->user->setFlash('success','Profile Update Success.');
                        $this->redirect(array('profileBsm/'));
                    }
                }
            }else if(empty($_FILES['Profile']['name'])) {
            	Yii::app()->user->setFlash('error','File Not Found.');
                $this->redirect(array('profileBsm/'));
            }
        }

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ProfileBsm::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
