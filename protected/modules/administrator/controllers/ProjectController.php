<?php

class ProjectController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return RolesMenu::actionRule('Project'); 
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Project;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->dibuat_oleh = Yii::app()->user->id; 
			$model->attributes=$_POST['Project'];

			if(!$model->validate()){
				$error = CJSON::decode(CActiveForm::validate($model));
				$msg = "";
				foreach( $error as $p ){
					$msg .= "<ul><li>".$p[0]."</li></ul>";
				}
				$response = ["status"=>'error','msg'=>$msg];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;				
			}

			if($model->save()){
				$response = ["status"=>'info','msg'=>'Data Berhasil Disimpan','redirect'=>Yii::app()->createAbsoluteUrl('administrator/project/upload/id/'.$model->id)];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;
			}
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->dibuat_oleh = Yii::app()->user->id; 
			$model->attributes=$_POST['Project'];
			if(!$model->validate()){
				$error = CJSON::decode(CActiveForm::validate($model));
				$msg = "";
				foreach( $error as $p ){
					$msg .= "<ul><li>".$p[0]."</li></ul>";
				}
				$response = ["status"=>'error','msg'=>$msg];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;				
			}

			if($model->save()){
				$response = ["status"=>'info','msg'=>'Data Berhasil Diupdate','redirect'=>Yii::app()->createAbsoluteUrl('administrator/project/view/id/'.$model->id)];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionUpload($id)
	{
		$model=$this->loadModel($id);

		$Project = new Project;
		$detail_Project = new ProjectDetail;
		$gambarlokasi = new FileLokasi;
		$timestamp = new CDbExpression('NOW()');
		$ProjectLokasi = Yii::getPathOfAlias('webroot').'/upload/project/';
		if (!empty($_FILES)) {
			$random = rand(0,99999999999);
			$tempFile = $_FILES['Project']['tmp_name']['file']; 
			$targetPath = $ProjectLokasi.$model->id.'/';  
			$file_name = $random.'_'.str_replace(' ' ,'_' ,$_FILES['Project']['name']['file']); 
			$targetFile =  $targetPath.$file_name;
		   	if (!is_dir($targetPath)) {
				try {
				  mkdir($targetPath, 0777, true);
				} catch(ErrorException $ex) {
				   echo "Error: " . $ex->getMessage();
				}
			}
	       		if(move_uploaded_file($tempFile,$targetFile)){
				$path = $targetPath.$file_name;
				$path_thumb = $targetPath.'thumb_'. $file_name;
				$thumb = Yii::app()->simpleImage->load($targetFile);
				$thumb->crop(400,300);
				$thumb->save($path_thumb);

				$round_thumb = $targetPath.'round_'. $file_name;
				$round = Yii::app()->simpleImage->load($targetFile);
				$round->crop(620,430);
				$round->save($round_thumb);


				$gambarlokasi->nama_file = $file_name;
				$gambarlokasi->tanggal_upload = $timestamp;
				if($gambarlokasi->save()){
					$detail_Project->project_id= $id;
					$detail_Project->gambar= $gambarlokasi->id;
					$detail_Project->no_urut= $id;
					$detail_Project->tanggal_upload= $timestamp;
					$detail_Project->save();

					if((int)$model->cover_image <= 0){
						$model->cover_image = $gambarlokasi->id;
						$model->save();

						$detail_Project->gambar = $gambarlokasi->id;
						$detail_Project->save();
					}
				}
		  	}
			return;
		}
		
		$this->render('upload',array(
			'model'=>$model,
		));
	}
	/* Function not support upload to centos linux */
	// public function actionUpload($id)
	// {

	// 	$model=$this->loadModel($id);

	// 	$Project = new Project;
	// 	$detail_Project = new ProjectDetail;
	// 	$gambarlokasi = new FileLokasi;

	// 	// $FileLokasi = new FileLokasi;
	// 	// $DetailProject = new ProjectDetail;
	// 	// $Project = new Project;
	// 	//$gambarlokasi = new FileLokasi;

	// 	$timestamp = new CDbExpression('NOW()');

	// 	$ProjectLokasi = Yii::getPathOfAlias('webroot').Yii::app()->params['Projectpic'];
	// 	//$ProjectLokasi = Yii::getPathOfAlias('webroot').'/upload/project/'; 
	// 	if (!empty($_FILES)) {
	// 		//print_r($_FILES['Project']);
	// 		$random = rand(0,99999999999);
	// 		$tempFile = $_FILES['Project']['tmp_name']['file']; 
	// 		$targetPath = $ProjectLokasi.$model->id.'/'; 
	// 		$file_name = $random.'_'.str_replace(' ' ,'_' ,$_FILES['Project']['name']['file']); 
	// 		$targetFile =  $targetPath.$file_name;
	// 	   	if (!file_exists($targetPath)) {
				
	// 			try {
	// 			  mkdir($targetPath, 0777, true);
	// 			} catch(ErrorException $ex) {
	// 			   echo "Error: " . $ex->getMessage();
	// 			}
	// 		}
	// 		if(move_uploaded_file($tempFile,$targetFile)){
	// 			$path = $targetPath.$file_name;
	// 			$path_thumb = $targetPath.'thumb_'. $file_name;
	// 			$thumb = Yii::app()->simpleImage->load($targetFile);
	// 			$thumb->crop(400,300);
	// 			$thumb->save($path_thumb);

	// 			$round_thumb = $targetPath.'round_'. $file_name;
	// 			$round = Yii::app()->simpleImage->load($targetFile);
	// 			$round->crop(156,156);
	// 			$round->save($round_thumb);


	// 			$gambarlokasi->nama_file = $file_name;
	// 			$gambarlokasi->tanggal_upload = $timestamp;
	// 			if($gambarlokasi->save()){
	// 				$detail_Project->Project_id= $id;
	// 				$detail_Project->gambar= $gambarlokasi->id;
	// 				$detail_Project->no_urut= $id;
	// 				$detail_Project->tanggal_upload= $timestamp;
	// 				$detail_Project->save();

	// 				if((int)$model->cover_image <= 0){
	// 					$model->cover_image = $gambarlokasi->id;
	// 					$model->save();

	// 					$detail_Project->gambar = $gambarlokasi->id;
	// 					$detail_Project->save();
	// 				}
	// 			}
	// 		}
	// 		return;
	// 	}
		
	// 	$this->render('upload',array(
	// 		'model'=>$model,
	// 	));
	// }

	public function actionImageDetail($file, $id){
		header('Content-Type: image/png, image/jpeg, image/jpg, image/gif');
		$path = Yii::getPathOfAlias('webroot').'/upload/project/'.$id.'/'.$file;
		readfile($path);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		ProjectDetail::model()->deleteAll(array(
		   'condition' => 'Project_id = :ID',
		   'params' => array(
		        ':ID' => $id
		   )
		));

		$this->loadModel($id)->delete();

		$dir = Yii::getPathOfAlias('webroot').'/upload/project/'.$id.'/';
		if (is_dir($dir)) {
	        $scn = scandir($dir);
	        foreach ($scn as $files) {
	            if ($files !== '.') {
	                if ($files !== '..') {
	                    if (!is_dir($dir . '/' . $files)) {
	                        unlink($dir . '/' . $files);
	                    } else {
	                        emptyDir($dir . '/' . $files);
	                        rmdir($dir . '/' . $files);
	                    }
	                }
	            }
	        }
	    }
		rmdir($dir);

		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$this->redirect(array('admin'));
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Project the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Project::model()->findByPk($id);

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadIdCover($id)
	{
		$model=Project::model()->findByPk(array('cover_image'=>$id));

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionPrimaryImage($id, $project_id){
		//print_r($Project_id);
		$model=$this->loadModel($project_id);

		$avatar=Project::model()->findByAttributes(array('id'=>$project_id));
		$avatar->attributes=array('cover_image'=>$id);
		$avatar->save();
		if ($avatar->save()) {
			Yii::app()->user->setFlash('success','Imaged Primary Key.');
            $this->redirect(array('upload','id'=>$model->id));
		}
	}

	public function actionDeleteImage($id, $project_id, $nama_file){
		$model=$this->loadModel($project_id);
		$getID = Project::getIdCover($id);
		$ProjectLokasi = Yii::getPathOfAlias('webroot').Yii::app()->params['projectpic'].$model->id.'/';
		
		$data = 0;
		foreach ($getID as $p) {
			$data = $p['cover_image'];
		}

		if ($data == $id) {
			Yii::app()->user->setFlash('success','Image could not be Delete Because Primary Key.');
            $this->redirect(array('upload','id'=>$model->id));
		}else{
			$path = $ProjectLokasi.$nama_file;
			$path_thumb = $ProjectLokasi.'thumb_'. $nama_file;
			$path_round = $ProjectLokasi.'round_'. $nama_file;
			if (file_exists($path)) {
				unlink($path);
			}
			if (file_exists($path_thumb)) {
				unlink($path_thumb);
			}
			if (file_exists($path_round)) {
				unlink($path_round);
			}
			
			ProjectDetail::model()->deleteAll(array(
			   'condition' => 'gambar = :ID',
			   'params' => array(
			        ':ID' => $id
			   )
			));

			FileLokasi::model()->deleteAll(array(
			   'condition' => 'id = :ID',
			   'params' => array(
			        ':ID' => $id
			   )
			));
			Yii::app()->user->setFlash('success','Imaged Delete Successfully.');
            $this->redirect(array('upload','id'=>$model->id));
		}
	}

	/**
	 * Performs the AJAX validation.
	 * @param Project $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='table-Project-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionExport(){
    $model=new Project;
    $model->unsetAttributes();
    $model->tanggal_project = $_GET['tanggal_project'];
    $model->dibuat_oleh = $_GET['dibuat_oleh'];
    $filename = 'Project ('.$model->tanggal_project.')';
    $factory = new CWidgetFactory();    
        $widget = $factory->createWidget($this, 'ext.EExcelView', array(
            'dataProvider'=>$model->search(),
            'grid_mode'=>'export',
            'title'=>'Project ('.$model->tanggal_project.')',
            'filename'=>$filename,
            'stream'=>true,
            'exportType'=>'Excel2007',
            'columns'=>array(
                  //'promotioncode',
            	array(
	                'name'=>'id',
	                'header'=>'ID',
	                'filter' => false,
                ), 
                array(
                  	'name'=>'cover_image',
                  	'header'=>'COVER IMAGE',
                  	'value'=>function($model){
									$data=$model->cover_image;
									$data_id = $model->id;
									if($data >= 1){
										$namafile =FileLokasi::model()->findByAttributes(array("id"=>$data))->nama_file;
										$imageUrl = Yii::app()->request->baseUrl.'/upload/project/'.$data_id.'/'.$namafile;
										$image = '<img class="img-fluid" src="'.$imageUrl.'" alt="" style="width:50px; height: 50px;" />';
										echo CHtml::link($image);
									}
								},
                  	'filter' => false,
                  	'sortable'=>false,
                ),
                array(
                  	'name'=>'nama_project',
                  	'header'=>'NAMA PROJECT',
                  	'filter' => false,
                  	'sortable'=>false,
                ),
                array(
                  	'name'=>'dropcaps',
                  	'header'=>'Dropcaps',
                  	'filter' => false,
                  	'sortable'=>false,
                ),
                array(
                  	'name'=>'keterangan_project',
                  	'header'=>'KETERANGAN PROJECT',
                  	'filter' => false,
                  	'sortable'=>false,
                ),
                array(
                  	'name'=>'tanggal_project',
                  	'header'=>'TANGGAL PROJECT',
                  	'value'=>'Yii::app()->dateFormatter->format("d MMM y",strtotime($data->tanggal_project))',
                  	'filter' => false,
                  	'sortable'=>false,
                ),
                array(
                  	'name'=>'dibuat_oleh',
                  	// 'value'=>'User::model()->findByAttributes(array("id"=>$data["dibuat_oleh"]))->nama_lengkap',
                  	'value'=>'User::model()->findByAttributes(array("id"=>$data["dibuat_oleh"]))->nama_lengkap',
                  	'header'=>'DIBUAT OLEH',
                  	'filter' => false,
                  	'sortable'=>false,
                ),
            ),
        ));
        $widget->init();
        $widget->run();
  }
}
