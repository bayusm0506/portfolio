<?php

class HomePageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return RolesMenu::actionRule('homePage'); 
	}

	public function actionIndex()
	{
		$model=$this->loadModel(1);
		if(isset($_POST['Home']))
		{
			$model->attributes = $_POST['Home'];
			if(!$model->validate()){
				$error = CJSON::decode(CActiveForm::validate($model));
				$msg = "";
				foreach( $error as $p ){
					$msg .= "<ul><li>".$p[0]."</li></ul>";
				}
				$response = ["status"=>'error','msg'=>$msg];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;				
			}
			
			if($model->save()){
				$response = ["status"=>'info','msg'=>'Data Berhasil di Update'];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;
			}
		}

		$this->render('home',array(
			'model'=>$model,
		));
	}

	public function actionLogo(){
		$model_logo=$this->loadModelLogo(1);
        $photoLokasi = Yii::getPathOfAlias('webroot').Yii::app()->params['photopic'];
        if(isset($_POST['HomeLogo'])){
            $model_logo->attributes=$_POST['HomeLogo'];
            if (!empty($_FILES['HomeLogo']['name'])) {
                $random = rand(0,99999999999);
                $tempFile = $_FILES['HomeLogo']['tmp_name']['logo']; 
                $targetPath = $photoLokasi;  
                $file_name = $random.'_'.str_replace(' ' ,'_' ,$_FILES['HomeLogo']['name']['logo']);
                $targetFile =  $targetPath.$file_name;
                if (file_exists($targetFile)) {
                	try {
                      echo "Success...";
                    } catch(ErrorException $ex) {
                       echo "Error: " . $ex->getMessage();
                    }
                }
                if(move_uploaded_file($tempFile,$targetFile)){
                    $path = $targetPath.$file_name;
                    $target = Yii::app()->simpleImage->load($targetFile);
                    $target->crop(128,128);
                    $target->save($path);

                    $round_thumb = $targetPath.'logo_'. $file_name;
					$round = Yii::app()->simpleImage->load($targetFile);
					$round->crop(123,72);
					$round->save($round_thumb);

                    $model_logo->logo = $file_name;
                    if($model_logo->save()){
                    	Yii::app()->user->setFlash('success','Logo Update Success.');
                        $this->redirect(array('homePage/'));
                    }
                }
            }
        }
        Yii::app()->user->setFlash('error','File Not Found.');
        $this->redirect(array('homePage/'));  
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Home::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogo($id)
	{
		$model=HomeLogo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
