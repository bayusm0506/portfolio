<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is displayed in the header section
	'title'=>'My Yii Blog',
	// this is used in error pages
	'adminEmail'=>'bayu@bayusm.com',
	// number of posts displayed per page
	// the copyright information displayed in the footer section
	'copyrightInfo'=>'Copyright &copy; 2009 by My Company.',
	'projectpic'=>'/upload/project/',
	'photopic'=>'/upload/photo/',
);
