<?php
	/**
	 * 
	 */
	class CPallet
	{
		public function init()
	    {
	        date_default_timezone_set("UTC");
	    }

	    function getRandomString($length = 50) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$string = '';

			for ($i = 0; $i < $length; $i++) {
				$string .= $characters[mt_rand(0, strlen($characters) - 1)];
			}

			return $string;
		}

		public function getRealIp()
		{
			if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
			{
				$ip=$_SERVER['HTTP_CLIENT_IP'];
			}
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
			{
				$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			else
			{
				$ip=$_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		}

		public function tgl_indo($tanggal){
			$dt = new DateTime($tanggal);
            $tanggal_ = $dt->format('Y-m-d');
			$bulan = array (
				1 =>'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
			$pecahkan = explode('-', $tanggal_);
			
			// variabel pecahkan 0 = tanggal
			// variabel pecahkan 1 = bulan
			// variabel pecahkan 2 = tahun
		 
			//return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
			return $bulan[ (int)$pecahkan[1] ] . ', ' . $pecahkan[0];
		}
	}
?>