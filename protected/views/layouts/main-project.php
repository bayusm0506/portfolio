<?php 
    $DataHome = Home::getHome();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php
                foreach ($DataHome as $p) {
                    echo $p['header']." - Blog Pribadi";
                }
            ?>
        </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="<?= Yii::app()->request->baseUrl; ?>/asset/images/icon.png" />
        <!-- 
        Rectangle Template 
        http://www.templatemo.com/preview/templatemo_439_rectangle
        -->
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/asset/css/normalize.css">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/asset/css/font-awesome.css">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/asset/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/asset/css/templatemo-style.css">
        <link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl; ?>/vendors/assets/css/message.css">

        <script src="<?= Yii::app()->request->baseUrl; ?>/asset/js/vendor/jquery-1.10.2.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="<?= Yii::app()->request->baseUrl; ?>/asset/js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="<?= Yii::app()->theme->baseUrl; ?>/vendors/assets/jquery/ajax.js"></script>
    </head>
    <body>
        <div class="messsage-container" style="display:none">
            <div class="loading-temp loading-show"></div>
            <div class="center-loading"><a href="#" style="font-size:24px" class="btn btn-default btn-sm">Waiting <i class="fa fa-spinner fa-spin" style="font-size:24px"></i></a></div>
        </div>
        <div class="message-container" style="display:none">
            <div class="center-message">
                <div class="alert alert-warning" id="container-m" role="alert"> 
                    <button id="remove-message" type="button" class="close">
                        <i class="fa fa-remove"></i>
                    </button> 
                    <div class="modal-header">
                      <h4 id="t-message"></h4> 
                    </div>
                    <div class="modal-body">
                      <p id="m-message" class="text-danger"></p> 
                    </div>
                </div> 
            </div>
        </div>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <div class="site-bg"></div>
        <div class="site-bg-overlay"></div>

        <!-- TOP HEADER -->
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <?php 
                        foreach ($DataHome as $p) {
                    ?>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p class="phone-info">Call me: <a href="#"><?= $p['phone'];?></a></p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="social-icons">
                            <ul>
                                <li><a href="<?= $p['link_fb'];?>" class="fa fa-facebook" target="<?=($p['link_fb']!='#' ? '_blank' : '')?>"></a></li>
                                <li><a href="<?= $p['link_twitter'];?>" class="fa fa-twitter" target="<?=($p['link_twitter']!='#' ? '_blank' : '')?>"></a></li>
                                <li><a href="<?= $p['link_linkedin'];?>" class="fa fa-linkedin" target="<?=($p['link_linkedin']!='#' ? '_blank' : '')?>"></a></li>
                                <li><a href="<?= $p['link_instagram'];?>" class="fa fa-instagram" target="<?=($p['link_instagram']!='#' ? '_blank' : '')?>"></a></li>
                                <li><a href="<?= $p['link_feeds'];?>" class="fa fa-rss" target="<?=($p['link_feeds']!='#' ? '_blank' : '')?>"></a></li>
                            </ul>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div> <!-- .top-header -->


        <div class="visible-xs visible-sm responsive-menu">
            <a href="#" class="toggle-menu">
                <i class="fa fa-bars"></i> Show Menu
            </a>
            <div class="show-menu">
                <ul class="main-menu">
                    <li>
                        <a class="show-1 active homebutton" href="#"><i class="fa fa-home"></i>Home</a>
                    </li>
                    <li>
                        <a class="show-2 aboutbutton" href="#"><i class="fa fa-user"></i>About Me</a>
                    </li>
                    <li>
                        <a class="show-3 projectbutton" href="#"><i class="fa fa-camera"></i>Gallery</a>
                    </li>
                    <li>
                        <a class="show-5 contactbutton" href="#"><i class="fa fa-envelope"></i>Contact</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container" id="page-content">
            <div class="row">
                
                <div class="col-md-9 col-sm-12 content-holder">
                    <!-- CONTENT -->
                    <div id="menu-container">
                        
                        <div class="logo-holder logo-top-margin">
                            <a href="https://bayusm.com" class="site-brand"><img src="<?= Yii::app()->request->baseUrl; ?>/asset/images/logo-bsm.png" alt="Logo BSM"></a>
                            <?php
                                foreach ($DataHome as $p) {
                                    echo "<p text-align='center'>".$p['header']."</p>";
                                }
                            ?>
                        </div>

                        <?php echo $content; ?>

                    </div>
                </div>


                <div class="col-md-3 hidden-sm">
                    
                    <nav id="nav" class="main-navigation hidden-xs hidden-sm">
                        <ul class="main-menu">
                            <li>
                                <a class="show-1 active homebutton" href="#"><i class="fa fa-home"></i>Home</a>
                            </li>
                            <li>
                                <a class="show-2 aboutbutton" href="#"><i class="fa fa-user"></i>About Me</a>
                            </li>
                            <li>
                                <a class="show-3 projectbutton" href="#"><i class="fa fa-camera"></i>Gallery</a>
                            </li>
                            <li>
                                <a class="show-5 contactbutton" href="#"><i class="fa fa-envelope"></i>Contact</a>
                            </li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>

        <!-- SITE-FOOTER -->
        <div class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>
                            Copyright &copy; 2015 - <?= date('Y'); ?> 
                            <?php
                                foreach ($DataHome as $p) {
                                    echo $p['header'];
                                }
                            ?>
                            , All right reserved
                        
                    <!-- | Design: <a href="http://www.templatemo.com" target="_parent"><span class="green">free templates</span></a> -->
                        </p>
                    </div>
                </div>
            </div>
        </div> <!-- .site-footer -->

        <script src="<?= Yii::app()->request->baseUrl; ?>/asset/js/plugins.js"></script>
        <script src="<?= Yii::app()->request->baseUrl; ?>/asset/js/main.js"></script>
        <script>
            $( document ).ready(function () {
                $(".moreBox").slice(0, 3).show();
                if ($(".blogBox:hidden").length != 0) {
                    $("#loadMore").show();
                }       

                $("#loadMore").on('click', function (e) {
                    e.preventDefault();
                    $(".moreBox:hidden").slice(0, 6).slideDown();
                    if ($(".moreBox:hidden").length == 0) {
                        $("#loadMore").fadeOut('slow');
                    }
                });                

                $('.close').click(function() {
                    $('.message-container').hide();
                });
            });
        </script>
        <!-- templatemo 439 rectangle -->
    </body>
</html>