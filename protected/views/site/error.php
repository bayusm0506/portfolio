<div class="home-section text-center">
    <div class="welcome-text">
        <h2>404 Error Page <strong>Not Found</strong></h2>
        <p>We couldn't find what you're looking for on.</p><br />
        <a href="https://bayusm.com"><button type="button" class="btn btn-info">BACK TO HOME</button></a>
    </div>
</div>