<div id="menu-1" class="homepage home-section text-center">

    <div class="welcome-text">

        <?php 

            foreach ($DataHome as $p) {

        ?>

        <h2>Hello, Selamat Datang <strong></strong></h2>

        <p><?= $p['content'];?></p>

        <?php 

            }

        ?>

        <?php $form=$this->beginWidget('CActiveForm', array(

            'id'=>'subscribe_form',

            'enableAjaxValidation'=>false,

        )); ?>

        <div class="subscribe-form">

            <div class="row">

                <fieldset class="col-md-offset-2 col-md-6">

                    <input name="Subscribe[email]" type="email" class="email" id="subscribe-email" placeholder="Write your email here..">

                </fieldset>

                <fieldset class="col-md-4 button-holder">

                    <input type="submit" class="button default" id="submit" value="Submit">

                </fieldset>

            </div>

            <p class="subscribe-text">Please subscribe your email for latest updates!</p>

        </div>

        <?php $this->endWidget(); ?>

    </div>

</div>
<script>
	$("form#subscribe_form").submit(function(){
        var _form = $(this);
        Ajax.run(_form.attr('action'),'POST',_form.serialize(),function(response){
            if(response.status == 'info' ){
                // $("#textter").text("Kami Akan segera merespon !!!");
                // _form[0].reset();
            }else if(response.status == 'error' ){
                    //$("#textter").text("Email Tidak Valid !!!");
            }
        });
        return false;
     });
</script>