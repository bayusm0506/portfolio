
<div id="menu-3" class="content gallery-section">
    <div class="box-content">
        <h3 class="widget-title">Past Projects</h3>
        <?php
            $SumData = $Jumproject[0]['project'];
            if (!empty($SumData)){
        ?>
        <div class="row">
            <?php
                $x = 0;
                foreach ($Project as $p){
            ?>
            <div class="col-md-4 col-sm-6 col-xs-12 blogBox moreBox" style="display: none;">
                <div class="project-item">
                    <a href="<?php echo Yii::app()->baseUrl; ?>/project/show/<?=$p['id']; ?>/<?=Project::slug($p['nama_project']); ?>">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/bsm/Showimage/?filename=<?php echo "/round_".$p['nama_file'];?>&id=<?php echo $p['id']; ?>" alt="<?php echo $p['nama_project']; ?>">
                        <div class="project-hover">
                            <h4><?php echo $p['nama_project']; ?></h4>
                        </div>
                    </a>
                </div>
            </div>
            <?php
                }
            ?>
        </div>
        <div class="project-pages" style="text-align: center;">
            <button id="loadMore" type="button" class="btn btn-primary">Load More</button>
        </div>
        <?php
            }else{
        ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <center>
                    <img src="<?= Yii::app()->request->baseUrl; ?>/asset/images/mario-bros.png" alt="Logo BSM" style="width:150px; height: 170px;">
                    <h1>I'M SORRY, THERE IS NO PROJECT</h1>
                </center>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
</div>