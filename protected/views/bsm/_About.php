<div id="menu-2" class="content about-section">
    <?php 
        foreach ($DataHome as $p) {
    ?>
    <div class="row">
        <div class="col-md-8 col-sm-8">
            <div class="box-content profile">
                <h3 class="widget-title">Learn about Me</h3>
                <div class="profile-thumb">
                    <img src="<?= Yii::app()->request->baseUrl; ?>/asset/images/image-bsm.jpg" alt="">
                </div>
                <div class="profile-content">
                    <h5 class="profile-name"><?= $p['header']; ?></h5>
                    <span class="profile-role"><?= $p['skill']; ?></span>
                    <p>
                        <?= $p['skill_content']; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="box-content">
                <h3 class="widget-title">Background</h3>
                <p><?= $p['background']; ?></p>
                <div class="about-social">
                    <ul>
                        <li><a href="<?= $p['link_fb'];?>" class="fa fa-facebook" target="<?=($p['link_fb']!='#' ? '_blank' : '')?>"></a></li>
                        <li><a href="<?= $p['link_twitter'];?>" class="fa fa-twitter" target="<?=($p['link_twitter']!='#' ? '_blank' : '')?>"></a></li>
                        <li><a href="<?= $p['link_linkedin'];?>" class="fa fa-linkedin" target="<?=($p['link_linkedin']!='#' ? '_blank' : '')?>"></a></li>
                        <li><a href="<?= $p['link_instagram'];?>" class="fa fa-instagram" target="<?=($p['link_instagram']!='#' ? '_blank' : '')?>"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php
        }
    ?>
    <div class="row">
        <div class="col-md-5 col-sm-5">
            <div class="box-content">
                <h3 class="widget-title">Our Studio</h3>
                <div class="project-item">
                    <img src="<?= Yii::app()->request->baseUrl; ?>/asset/images/software-enginering.jpg" alt="">
                    <div class="project-hover">
                        <h4>Software Enginering</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-sm-7">
            <div class="box-content">
                <h3 class="widget-title">Technical Skills</h3>
                <ul class="progess-bars">
                    <?php
                        foreach ($DataSkill as $s) {
                    ?>
                    <li>
                        <span><?= $s['skill'];?></span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="<?= $s['progress'];?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $s['progress'];?>%;"></div>
                        </div>
                    </li>
                    <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>