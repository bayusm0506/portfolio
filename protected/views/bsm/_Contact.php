<div id="menu-4" class="content contact-section">

    <div class="row">

        <div class="col-md-8 col-sm-8">

            <div class="box-content">

                <h3 class="widget-title">Send Me A Message</h3>

                <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'contact_form',
                        'enableAjaxValidation'=>false,
                    ));
                ?>

                    <div class="contact-form">
                        <fieldset>
                            <input name="Contact[name]" type="text" class="name" id="name" placeholder="Name...">
                        </fieldset> 
                        <fieldset>
                            <input name="Contact[email]" type="email" class="email" id="email" placeholder="Email...">
                        </fieldset> 
                        <fieldset>
                            <input name="Contact[subject]" type="text" class="subject" id="subject" placeholder="Subject...">
                        </fieldset>
                        <fieldset>
                            <textarea name="Contact[message]" id="message" cols="30" rows="4" placeholder="Message.."></textarea>
                        </fieldset>
                        <fieldset>
                            <input type="submit" class="button" id="button" value="Send Message">
                        </fieldset>
                    </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>

        <div class="col-md-4 col-sm-4">

            <div class="box-content">

                <?php

                    foreach ($DataHome as $p) {

                ?>

                <h3 class="widget-title">Stay In Touch</h3>

                <p><?= $p['stay_touch']; ?></p>

                <div class="about-social">

                    <ul>

                        <li><a href="<?= $p['link_fb'];?>" class="fa fa-facebook" target="<?=($p['link_fb']!='#' ? '_blank' : '')?>"></a></li>

                        <li><a href="<?= $p['link_twitter'];?>" class="fa fa-twitter" target="<?=($p['link_twitter']!='#' ? '_blank' : '')?>"></a></li>

                        <li><a href="<?= $p['link_linkedin'];?>" class="fa fa-linkedin" target="<?=($p['link_linkedin']!='#' ? '_blank' : '')?>"></a></li>

                        <li><a href="<?= $p['link_instagram'];?>" class="fa fa-instagram" target="<?=($p['link_instagram']!='#' ? '_blank' : '')?>"></a></li>

                    </ul>

                </div>

                <?php

                    }

                ?>

            </div>

        </div>

    </div>

</div>
<script>
	$("form#contact_form").submit(function(){
    	var _form = $(this);
    	Ajax.run(_form.attr('action'),'POST',_form.serialize(),function(response){
    		if(response.status == 'info' ){
        			// $("#textter").text("Kami Akan segera merespon !!!");
        			// _form[0].reset();
    		}else if(response.status == 'error' ){
        			//$("#textter").text("Email Tidak Valid !!!");
    		}
    	});
    	return false;
	});
</script>
