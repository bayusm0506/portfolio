<?php



/**

 * This is the model class for table "{{table_project}}".

 *

 * The followings are the available columns in table '{{table_project}}':

 * @property string $id

 * @property string $nama_project

 * @property string $keterangan_project

 * @property string $tanggal_project

 * @property string $cover_image

 * @property string $dibuat_oleh

 * @property string $tanggal_upload

 */

class Project extends CActiveRecord

{

	/**

	 * @return string the associated database table name

	 */

	public function tableName()

	{

		return '{{table_project}}';

	}



	/**

	 * @return array validation rules for model attributes.

	 */

	public function rules()

	{

		// NOTE: you should only define rules for those attributes that

		// will receive user inputs.

		return array(

			array('nama_project, dropcaps, tanggal_project', 'required'),

			array('nama_project', 'length', 'max'=>255),

			array('cover_image, dibuat_oleh', 'length', 'max'=>20),

			array('keterangan_project, tanggal_project, dropcaps', 'safe'),

			// The following rule is used by search().

			// @todo Please remove those attributes that should not be searched.

			array('id, nama_project, keterangan_project, tanggal_project, cover_image, dibuat_oleh, tanggal_upload, dropcaps', 'safe', 'on'=>'search'),

		);

	}



	/**

	 * @return array relational rules.

	 */

	public function relations()

	{

		// NOTE: you may need to adjust the relation name and the related

		// class name for the relations automatically generated below.

		return array(

		);

	}



	/**

	 * @return array customized attribute labels (name=>label)

	 */

	public function attributeLabels()

	{

		return array(

			'id' => 'ID',

			'nama_project' => 'Nama project',

			'keterangan_project' => 'Keterangan project',

			'dropcaps' => 'Dropcaps',

			'tanggal_project' => 'Tanggal project',

			'tanggal_pencarian' => 'Tanggal project',

			'cover_image' => 'Cover Image',

			'dibuat_oleh' => 'Dibuat Oleh',

			'tanggal_upload' => 'Tanggal Upload',

		);

	}



	/**

	 * Retrieves a list of models based on the current search/filter conditions.

	 *

	 * Typical usecase:

	 * - Initialize the model fields with values from filter form.

	 * - Execute this method to get CActiveDataProvider instance which will filter

	 * models according to data in model fields.

	 * - Pass data provider to CGridView, CListView or any similar widget.

	 *

	 * @return CActiveDataProvider the data provider that can return the models

	 * based on the search/filter conditions.

	 */

	public function search()

	{

		// @todo Please modify the following code to remove attributes that should not be searched.



		$criteria=new CDbCriteria;



		$criteria->compare('id',$this->id,true);

		$criteria->compare('nama_project',$this->nama_project,true);

		$criteria->compare('keterangan_project',$this->keterangan_project,true);

		$criteria->compare('dropcaps',$this->dropcaps,true);

		$criteria->compare('tanggal_project',$this->tanggal_project,true);

		$criteria->compare('cover_image',$this->cover_image,true);

		$criteria->compare('dibuat_oleh',$this->dibuat_oleh,true);

		$criteria->compare('tanggal_upload',$this->tanggal_upload,true);



		return new CActiveDataProvider($this, array(

			'criteria'=>$criteria,

		));

	}



	/**

	 * Returns the static model of the specified AR class.

	 * Please note that you should have this exact method in all your CActiveRecord descendants!

	 * @param string $className active record class name.

	 * @return Project the static model class

	 */

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}

	public static function getProject(){



		$sql = "SELECT A.id, A.tanggal_project, A.nama_project, A.keterangan_project, C.nama_file

				FROM t_table_project A, t_table_project_detail B, t_file_lokasi C

			    WHERE A.cover_image = C.id

			    AND B.gambar = C.id 

			    ORDER BY A.id";

		$command = Yii::app()->db->createCommand($sql);

		return $dataReader = $command->queryAll();

	}



	public static function getNameProject($id){



		$sql = "SELECT A.id, A.nama_project, B.nama_file, A.dropcaps, A.keterangan_project, A.tanggal_project

				FROM t_table_project A, t_file_lokasi B

			    WHERE A.cover_image = B.id AND A.id=$id

				";

		$command = Yii::app()->db->createCommand($sql);

		//$command->bindParam(":tahun", $tahun, PDO::PARAM_STR);

		return $dataReader = $command->queryAll();

	}



	public static function getIdCover($id){



		$sql = "SELECT cover_image FROM t_table_project 

                WHERE cover_image = $id

				";

		$command = Yii::app()->db->createCommand($sql);

		//$command->bindParam(":tahun", $tahun, PDO::PARAM_STR);

		return $dataReader = $command->queryAll();

	}



	public static function updatePrimaryImage($id, $project_id, $params=array()){

		return Yii::app()->db->createCommand()->update(

		        't_table_project', 

		        array(

		            'cover_image'=>'$id',

		        ), 

		        'id=:id', 

		        array(':id'=>$project_id)

		    );

	}



	public static function slug($text)

	{

	    // replace non letter or digits by -

	    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	 

	    // trim

	    $text = trim($text, '-');

	 

	    // transliterate

	    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	 

	    // lowercase

	    $text = strtolower($text);

	 

	    // remove unwanted characters

	    $text = preg_replace('~[^-\w]+~', '', $text);

	 

	    if (empty($text))

	    {

	        return 'n-a';

	    }

	 

	    return $text;

	} 



	public static function getProjectPage(){



		$sql = "SELECT A.id, A.nama_project, A.keterangan_project, A.tanggal_project, 

				B.project_id, B.gambar, B.no_urut, 

				C.tanggal_upload, C.nama_file 

				FROM t_table_project A, t_table_project_detail B, t_file_lokasi C 

				WHERE A.id = B.project_id 

				AND B.gambar = C.id 

				GROUP BY B.project_id DESC";

		return $sql;

	}



	public static function getImageProject($id){

		//$oci = Yii::app()->db;

		$sql = "SELECT A.cover_image, B.nama_file, B.id

				FROM t_table_project A, t_file_lokasi B

				WHERE B.id = $id 

				AND A.cover_image = B.id";

		$command = Yii::app()->db->createCommand($sql);

		//$command->bindParam(":tahun", $tahun, PDO::PARAM_STR);

		return $dataReader = $command->queryAll();

	}

	

	public static function getImageTimeLast($id){

		//$oci = Yii::app()->db;

		$sql = "SELECT tanggal_upload AS imagetime

				FROM t_table_project_detail 

				WHERE project_id = $id ORDER BY tanggal_upload DESC";

		$command = Yii::app()->db->createCommand($sql);

		//$command->bindParam(":tahun", $tahun, PDO::PARAM_STR);

		return $dataReader = $command->queryRow();

	}



	public static function getSumProject(){

		$sql = "SELECT COUNT(*) AS project

				FROM t_table_project";

		$command = Yii::app()->db->createCommand($sql);

		return $dataReader = $command->queryAll();

	}



	public static function time_elapsed_string($datetime, $full = false) {

	    $now = new DateTime;

	    $ago = new DateTime($datetime);

	    $diff = $now->diff($ago);



	    $diff->w = floor($diff->d / 7);

	    $diff->d -= $diff->w * 7;



	    $string = array(

	        'y' => 'year',

	        'm' => 'month',

	        'w' => 'week',

	        'd' => 'day',

	        'h' => 'hour',

	        'i' => 'minute',

	        's' => 'second',

	    );

	    foreach ($string as $k => &$v) {

	        if ($diff->$k) {

	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');

	        } else {

	            unset($string[$k]);

	        }

	    }



	    if (!$full) $string = array_slice($string, 0, 1);

	    return $string ? implode(', ', $string) . ' ago' : 'just now';

	}



	public static function getXmax(){

		$sql='SELECT * FROM t_table_project';

 

		 $dataProvider=new CSqlDataProvider($sql,array(

		   'keyField' => 'id',

		   'totalItemCount'=>Project::model()->count(),

		   'pagination'=>array(

		       'pageSize'=>8,

		    ),

		 ));

	}

}

