<?php

class BsmController extends Controller
{
	public $layout='column2';
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionIndex()
	{
		$model=new Subscribe;  
		$contact=new Contact;

        if(isset($_POST['Subscribe']))
		{
			$model->attributes = $_POST['Subscribe'];
			if(!$model->validate()){
				$error = CJSON::decode(CActiveForm::validate($model));
				$msg = "";
				foreach( $error as $p ){
					$msg .= "<ul><li>".$p[0]."</li></ul>";
				}
				$response = ["status"=>'error','msg'=>$msg];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;				
			}

			$data = 0;
			$emailDouble = Subscribe::getDoubleEmail($model->email);
			foreach ($emailDouble as $p) {
				$data = $p['email'];
			}
			if ($data != '') {
				$response = ["status"=>'error','msg'=>'Maaf, email ini sudah teregister sebelumnya, silahkan masukkan email lain'];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;
			}
			
			if($model->save()){
				$response = ["status"=>'info','msg'=>'Thanks your subscribe in my website'];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;
			}
		}

		if(isset($_POST['Contact']))
		{
			$contact->attributes = $_POST['Contact'];
			if(!$contact->validate()){
				$error = CJSON::decode(CActiveForm::validate($contact));
				$msg = "";
				foreach( $error as $p ){
					$msg .= "<ul><li>".$p[0]."</li></ul>";
				}
				$response = ["status"=>'error','msg'=>$msg];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;				
			}

			$data = 0;
			$emailDouble = Contact::getDoubleEmail($contact->email);
			foreach ($emailDouble as $p) {
				$data = $p['email'];
			}
			if ($data != '') {
				$response = ["status"=>'error','msg'=>'Maaf, email ini sudah teregister sebelumnya, silahkan masukkan email lain'];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;
			}
			
			if($contact->save()){
				$response = ["status"=>'info','msg'=>'Saya Akan Segera Menghubungi Anda Kembali'];
				echo CJSON::encode($response);
				Yii::app()->end();
				return;
			}
		}

		$this->render('index',[
			'DataHome' => Home::getHome(),
			'DataSkill' => Skill::getSkill(),
			'Project' => Project::getProject(),
			'PageProject' => Project::getProjectPage(),
			'Jumproject' => Project::getSumProject()
		]);
	}

	public function actionShowimage($filename = false, $id)
	{
		$path = Yii::getPathOfAlias('webroot').'/upload/project/'.$id.'/';
	    if ($filename)
	    {
	        // $path =  Yii::getPathOfAlias('application.images'). '/';
	        $path = Yii::getPathOfAlias('webroot').'/upload/project/'.$id.'/'; 
	        if (file_exists($path.$filename))
	        {
	            Yii::app()->request->sendFile(
	                $filename,
	                file_get_contents($path.$filename)
	            );
	        } else {
	            echo "File not found!";
	        }
	    }
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}